const { Router } = require('express');
const UserService = require('../services/userService');
const { UserRepository } = require('../repositories/userRepository');
const { createUserValid, updateUserValid, checkUserAvailability } = require('../middlewares/user.validation.middleware');
const { responseMiddleware } = require('../middlewares/response.middleware');

const router = Router();

// TODO: Implement route controllers for user
router.get('/', (req, res, next) => {
    const users = UserRepository.getAll();
    res.json(users);
})

router.get('/:id', checkUserAvailability, (req, res, next) => {
    const userId = req.params.id;
    const user = UserService.search({'id': userId});
    res.json(user);
})

router.post('/', createUserValid, (req, res, next) => {
    const newUser = UserRepository.create(req.body);
    res.json(newUser);
})

router.put('/:id', updateUserValid, (req, res, next) => {
    const newUserData = UserRepository.update(req.params.id, req.body);
    res.json(newUserData);
})

router.delete('/:id', checkUserAvailability, (req, res, next) => {
    const deleteResult = UserRepository.delete(req.params.id);
    if (deleteResult.length > 0) {
        res.json(deleteResult);
    } else {
        res.status(400).json({'error': true, 'message': 'User not found'});
    }
})

module.exports = router;