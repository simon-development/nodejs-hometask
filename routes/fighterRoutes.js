const { Router } = require('express');
const FighterService = require('../services/fighterService');
const { FighterRepository } = require('../repositories/fighterRepository');
const { responseMiddleware } = require('../middlewares/response.middleware');
const { createFighterValid, updateFighterValid, checkFighterAvailability } = require('../middlewares/fighter.validation.middleware');

const router = Router();

// TODO: Implement route controllers for fighter
router.get('/', (req, res, next) => {
    const fighters = FighterRepository.getAll();
    res.json(fighters);
})

router.get('/:id', checkFighterAvailability, (req, res, next) => {
    const fighterId = req.params.id;
    const fighter = FighterService.search({'id': fighterId});
    res.json(fighter);
})

router.post('/', createFighterValid, (req, res, next) => {
    const newFighter = FighterRepository.create(req.body);
    res.json(newFighter);
})

router.put('/:id', updateFighterValid, (req, res, next) => {
    const newFighterData = FighterRepository.update(req.params.id, req.body);
    res.json(newFighterData);
})

router.delete('/:id', checkFighterAvailability, (req, res, next) => {
    const deleteResult = FighterRepository.delete(req.params.id);
    if (deleteResult.length > 0) {
        res.json(deleteResult);
    } else {
        res.status(400).json({'error': true, 'message': 'Fighter not found'});
    }
})

module.exports = router;