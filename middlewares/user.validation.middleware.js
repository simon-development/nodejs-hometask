const { user } = require('../models/user');
const UserService = require('../services/userService');

const createUserValid = (req, res, next) => {
    // TODO: Implement validatior for user entity during creation
    let possibleErrors = validateUserData(req);
    if (possibleErrors) {
        return res.status(400).json({
            "error": true,
            "message": possibleErrors
        })
    }
    next();
}

const updateUserValid = (req, res, next) => {
    // TODO: Implement validatior for user entity during update
    let possibleErrors = validateUserData(req);
    if (possibleErrors) {
        return res.status(400).json({
            "error": true,
            "message": possibleErrors
        })
    }
    next();
}

const checkUserAvailability = (req, res, next) => {
    const user = UserService.search({'id': req.params.id})
    if (!user) {
        return res.status(404).json({
            "error": true,
            "message": 'User is not found'
        })
    }
    next();
}

const checkForRedundantFields = (obj, validFields) => {
    let errorMessage = '';
    Object.keys(obj).forEach((key) => validFields.includes(key) || (errorMessage = 'We don\'t need extra fields\n'));
    return errorMessage;
}

const removeRedundantExceptGiven = (obj, fieldsToKeep) => {
    Object.keys(obj).forEach((key) => fieldsToKeep.includes(key) || delete obj[key]);
    delete obj['id'];
}

const validateUserData = (req) => {
    let fields = Object.keys(user);
    delete fields['id'];
    let errorMessage = checkForRedundantFields(req.body, fields)
    const userId = req.params.id;
    const data = req.body;
    const password = data.password;
    if (!validateEmail(data.email)) {
        errorMessage += 'Email is not valid\n';
    }

    const userWithTheSameEmail = UserService.search({'email': data.email});
    if (userWithTheSameEmail && !(userId && userWithTheSameEmail.id === userId)) {
        errorMessage += 'Email is already used \n';
    }

    if (!validatePhone(data.phoneNumber)) {
        errorMessage += 'Phone is not in +380xxxxxxxxx format\n';
    }

    const userWithTheSamePhone = UserService.search({'phoneNumber': data.phoneNumber});
    if (userWithTheSamePhone && !(userId && userWithTheSamePhone.id === userId)) {
        errorMessage += 'Phone is already used \n';
    }

    if (! password || password.length < 3) {
        errorMessage += 'Password is less than 3 symbols\n';
    }
    if (! data.firstName || ! data.lastName) {
        errorMessage += 'Fill in your first name and last name\n';
    }
    return errorMessage;
}

const validatePhone = (phone) => {
    const phoneIsValid = phone &&
        phone.length == 13 &&
        phone.substring(0, 4) == '+380' &&
        /^\d{9}$/.test(phone.substring(4))
        ;
    return phoneIsValid;
}

const validateEmail = (email) => {
    var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

    return re.test(email) && email.substring(email.length - 10, email.length) === '@gmail.com';
}

exports.createUserValid = createUserValid;
exports.updateUserValid = updateUserValid;
exports.checkUserAvailability = checkUserAvailability;