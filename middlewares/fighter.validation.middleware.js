const { fighter } = require('../models/fighter');
const FighterService = require('../services/fighterService');

const createFighterValid = (req, res, next) => {
    // TODO: Implement validatior for fighter entity during creation
    const result = validateFighterData(req);

    if (result) {
        return res.status(400).json({
            "error": true,
            "message": result
        })
    }
    next();
}

const updateFighterValid = (req, res, next) => {
    // TODO: Implement validatior for fighter entity during update
    const result = validateFighterData(req);

    if (result) {
        return res.status(400).json({
            "error": true,
            "message": result
        })
    }
    next();
}

const checkForRedundantFields = (obj, validFields) => {
    let errorMessage = '';
    Object.keys(obj).forEach((key) => validFields.includes(key) || (errorMessage = 'We don\'t need extra fields\n'));
    return errorMessage;
}

const removeRedundantExceptGiven = (obj, fieldsToKeep) => {
    Object.keys(obj).forEach((key) => fieldsToKeep.includes(key) || delete obj[key]);
    delete obj['id'];
}

const checkFighterAvailability = (req, res, next) => {
    const fighter = FighterService.search({'id': req.params.id})
    if (!fighter) {
        return res.status(404).json({
            "error": true,
            "message": 'Fighter is not found'
        })
    }
    next();
}

const validateFighterData = (req) => {
    let fields = Object.keys(fighter);
    delete fields['id'];
    let errorMessage = checkForRedundantFields(req.body, fields)
    const fighterId = req.params.id;
    const data = req.body;
    const power = data.power;
    const defense = data.defense;
    if (! data.name) {
        errorMessage += 'Fill in your fighter\'s name\n';
    }

    const fighterWithTheSameName = FighterService.search({'name': data.name});
    if (fighterWithTheSameName && !(fighterId && fighterWithTheSameName.id === fighterId)) {
        errorMessage += 'Name is already used \n';
    }

    if (!power || typeof (+power) != 'number' || power > 100 || power < 1) {
        errorMessage += 'Fighter\'s power is invalid\n';
    }
    if (!defense || typeof (+defense) != 'number' || defense > 10 || defense < 1) {
        errorMessage += 'Fighter\'s defense is invalid\n';
    }

    return errorMessage;
}

exports.createFighterValid = createFighterValid;
exports.updateFighterValid = updateFighterValid;
exports.checkFighterAvailability = checkFighterAvailability;